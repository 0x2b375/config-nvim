# config-nvim

Neovim configuration in Lua which makes use of LSP and Treesitter.

<div align="center">

![screenshot.png](screenshot.png)

</div>

