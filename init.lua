local utils = require('utils')
local opt = utils.opt
local map = utils.map

-- settings
opt('b', 'expandtab', true)                           -- use spaces instead of tabs
opt('b', 'shiftwidth', 4)                             -- size of an indent
opt('b', 'smartindent', true)                         -- insert indents automatically
opt('b', 'tabstop', 4)                                -- number of spaces tabs count for
opt('o', 'hidden', true)                              -- enable modified buffers in background
opt('o', 'shortmess', vim.o.shortmess .. 'c')         -- remove unwanted messages
opt('o', 'ignorecase', true)                          -- ignore case for search
opt('o', 'joinspaces', false)                         -- no double spaces with join after a dot
opt('o', 'scrolloff', 8)                              -- lines of context
opt('o', 'shiftround', true)                          -- round indent for < and >
opt('o', 'sidescrolloff', 8)                          -- columns of context
opt('o', 'smartcase', true)                           -- don't ignore case with capitals
opt('o', 'splitbelow', true)                          -- put new windows below current
opt('o', 'splitright', true)                          -- put new windows right of current
opt('o', 'wildmode', 'longest:full')                  -- command-line completion mode
opt('w', 'number', true)                              -- print line number
opt('w', 'wrap', false)                               -- disable line wrap
opt('o', 'inccommand', 'nosplit')                     -- commands effect will be shown incrementally
opt('o', 'pumblend', 20)                              -- transparency for popups
opt('o', 'switchbuf', 'uselast')                      -- from quickfix to prev used window
opt('o', 'mouse', 'nvih')                             -- mouse integration
opt('w', 'cursorline', true)                          -- highlight current line
opt('o', 'completeopt', 'menuone,noselect')           -- autocompletion options for nvim-compe
opt('w', 'signcolumn', 'yes')                         -- always show left column for git signs

-- globals
vim.g.mapleader = ' '
vim.g.maplocalleader = ','
vim.g.kommentary_create_default_mappings = false

-- keymaps
map('n', '<C-L>', [[<cmd>nohlsearch<CR><C-L>]], { silent = true })

-- plugins
require('plugins')

-- colors
opt('o', 'termguicolors', true)                       -- true color support
vim.g.moonflyCursorColor = 1
vim.g.moonflyUnderlineMatchParen = 1
vim.api.nvim_command('colorscheme nightfly')

