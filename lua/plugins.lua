local install_path = vim.fn.stdpath('data')..'/site/pack/packer/opt/packer.nvim'
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  vim.api.nvim_command('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
end

vim.cmd [[packadd packer.nvim]]
return require('packer').startup(function(use)
  -- Package manager
  use { 'wbthomason/packer.nvim', opt = true, }

  -- Treesitter
  use { 
    'nvim-treesitter/nvim-treesitter',
    requires = {
      'nvim-treesitter/nvim-treesitter-refactor',
      'nvim-treesitter/nvim-treesitter-textobjects',
    },
    config = [[require('config.treesitter')]],
    run = ':TSUpdate',
  }

  -- Autocompletion
  use { 'hrsh7th/nvim-compe', config = [[require('config.compe')]] }

  -- LSP
  use { 'neovim/nvim-lspconfig', config = [[require('config.lsp')]] }

  -- Previews
  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      'nvim-lua/popup.nvim', 
      'nvim-lua/plenary.nvim',
      { 'kyazdani42/nvim-web-devicons', opt = true }
    },
    config = [[require('config.telescope')]],
  }

  -- Support for languages treesitter doesn't support
  use { 'sheerun/vim-polyglot', config = [[require('config.polyglot')]], }

  -- Close brackets automatically
  use 'cohama/lexima.vim'

  -- Git integration
  use {
    'lewis6991/gitsigns.nvim',
    requires = { 'nvim-lua/plenary.nvim' },
    config = function()
      require('gitsigns').setup()
    end,
  }

  use {
    'TimUntersberger/neogit',
    config = function()
      require('neogit').setup()
    end,
  }
  
  -- Color theme
  use { 'bluz71/vim-nightfly-guicolors', }

  -- Statusline
  use {
    'hoob3rt/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true },
    config = [[require('config.lualine')]],
  }

  -- Commenting
  use { 
    'b3nj5m1n/kommentary', 
    config = [[require('config.kommentary')]]
  }
end)

