require('compe').setup {
  source = {
    buffer = true,
    nvim_lsp = true,
    nvim_lua = true,
    nvim_treesitter = true,
  },
}

local utils = require('utils')
local map = utils.map

map('i', '<C-Space>', [[compe#complete()]], { silent = true, expr = true})
map('i', '<CR>', [[compe#confirm(lexima#expand('<LT>CR>', 'i'))]], { silent = true, expr = true})
map('i', '<C-e>', [[compe#close('<C-e>')]], { silent = true, expr = true})
map('i', '<C-f>', [[compe#scroll({ 'delta': +4 })]], { silent = true, expr = true})
map('i', '<C-b>', [[compe#scroll({ 'delta': -4 })]], { silent = true, expr = true})

