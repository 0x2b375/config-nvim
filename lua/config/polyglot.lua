vim.g.polyglot_disabled = {
  'sensible',

  'sh.plugin', 'c.plugin', 'cpp.plugin', 'fennel.plugin',
  'go.plugin', 'html.plugin', 'json.plugin', 'jsonc.pluign',
  'julia.plugin', 'lua.plugin', 'python.plugin', 'rst.plugin',
  'ruby.plugin', 'rust.plugin', 'toml.plugin', 'yaml.plugin',
  'zig.plugin',
}
