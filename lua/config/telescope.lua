local utils = require('utils')
local map = utils.map

map('n', '<leader>sf', '<cmd>Telescope find_files<CR>')
map('n', '<leader>sg', '<cmd>Telescope live_grep<CR>')
map('n', '<leader>sb', '<cmd>Telescope buffers<CR>')
map('n', '<leader>sh', '<cmd>Telescope help_tags<CR>')
map('n', '<leader>ss', '<cmd>Telescope builtin<CR>')
