require('nvim-treesitter.configs').setup {
  -- after adding a new language, disable it's syntax highlighting in polyglot.lua
  ensure_installed = { 
    'bash', 'c', 'cpp', 'comment', 'fennel',
    'go', 'html', 'java', 'json', 'jsonc', 'julia',
    'lua', 'python', 'query', 'regex', 'rst',
    'ruby', 'rust', 'toml', 'yaml', 'zig',
  },
  highlight = { enable = true },
  indent = { enable = false },  -- // TODO: re-enable after https://github.com/nvim-treesitter/nvim-treesitter/pull/1020
  incremental_selection = { enable = true },
  highlight_definitions = { enable = true },
  highlight_current_scope = { enable = true },
  smart_rename = { enable = true },
}

