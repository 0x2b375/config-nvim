local utils = require('utils')
local map = utils.map

map('n', '<leader>cc', '<Plug>kommentary_line_default', { noremap = false })
map('n', '<leader>c', '<Plug>kommentary_motion_default', { noremap = false })
map('v', '<leader>c', '<Plug>kommentary_visual_default', { noremap = false })

